# app-hepatite-predict
App integrado com aplicação de aprendizado de máquina para predição de doença hepática.

>  Bibliotecas usadas
>  *  Firebase Authentication
> *  React-Navigation
> *  react-native-vector-icons
> *  axios



<img src="https://gitlab.com/alexgcunha/app-hepatite-predict/raw/master/readme-image/Screenshot_20190620-234550.jpg" width="300" height="500" />
<img src="https://gitlab.com/alexgcunha/app-hepatite-predict/raw/master/readme-image/Screenshot_20190616-154341.jpg" width="300" height="500" />
<img src="https://gitlab.com/alexgcunha/app-hepatite-predict/raw/master/readme-image/Screenshot_20190703-172739.jpg" width="300" height="500" />
<img src="https://gitlab.com/alexgcunha/app-hepatite-predict/raw/master/readme-image/Screenshot_20190703-173617.jpg" width="300" height="500" />